# Unattended Fedora 38 Installation with Cloud-Init

# Use text mode install
text

# Configure installation method
url --mirrorlist="https://mirrors.fedoraproject.org/mirrorlist?repo=fedora-38&arch=x86_64"
repo --name=fedora-updates --mirrorlist="https://mirrors.fedoraproject.org/mirrorlist?repo=updates-released-f38&arch=x86_64" --cost=0

# Disable firstboot
firstboot --disable

# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'

# System language
lang en_US.UTF-8

# Network configuration with DHCP
network --bootproto=dhcp --onboot=on

# Firewall configuration
firewall --enabled --ssh

# SELinux configuration
selinux --enforcing

# Timezone
timezone UTC --utc

# System bootloader configuration
bootloader --location=mbr --boot-drive=vda

# Clear the Master Boot Record
zerombr

# Automatically partitioning
autopart

# Use all available space
clearpart --all --initlabel

# Ignore second drive
ignoredisk --drives=vdb

# Reboot after installation
reboot

# Enable and configure services, including cloud-init
services --enabled=cloud-init,cloud-config,cloud-final,cloud-init-local

# Lock the root password
rootpw --lock

# Package selection (minimal install)
%packages
@core
cloud-init
%end
