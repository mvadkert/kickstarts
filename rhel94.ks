# Unattended RHEL-9.4.0 Installation with Cloud-Init

# Use text mode install
text

# Configure installation method
url --url="http://download.devel.redhat.com/rhel-9/composes/RHEL-9/RHEL-9.4.0-20231214.11/compose/BaseOS/x86_64/os"
repo --name=rhel-baseos --baseurl="http://download.devel.redhat.com/rhel-9/composes/RHEL-9/RHEL-9.4.0-20231214.11/compose/BaseOS/x86_64/os"
repo --name=rhel-appstream --baseurl="http://download.devel.redhat.com/rhel-9/composes/RHEL-9/RHEL-9.4.0-20231214.11/compose/AppStream/x86_64/os"

# Disable firstboot
firstboot --disable

# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'

# System language
lang en_US.UTF-8

# Network configuration with DHCP
network --bootproto=dhcp --onboot=on

# Firewall configuration
firewall --enabled --ssh

# SELinux configuration
selinux --enforcing

# Timezone
timezone UTC --utc

# System bootloader configuration
bootloader --location=mbr --boot-drive=vda

# Clear the Master Boot Record
zerombr

# Automatically partitioning
autopart

# Use all available space
clearpart --all --initlabel

# Ignore second drive
ignoredisk --drives=vdb

# Reboot after installation
reboot

# Enable and configure services, including cloud-init
services --enabled=cloud-init,cloud-config,cloud-final,cloud-init-local

# Lock the root password
rootpw --lock

# Package selection (minimal install)
%packages
@core
cloud-init
%end
